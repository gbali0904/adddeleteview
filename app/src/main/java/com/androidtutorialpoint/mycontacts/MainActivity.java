package com.androidtutorialpoint.mycontacts;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    private LinearLayout parentLinearLayout;
    int i=1;
    private TextView textView;
    List list=new ArrayList();
    private View rowView;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        parentLinearLayout = (LinearLayout) findViewById(R.id.parent_linear_layout);

    }
    public void onAddField(View v) {
        LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        rowView = inflater.inflate(R.layout.field, null);
        // Add the new row before the add field button.
        parentLinearLayout.addView(rowView, parentLinearLayout.getChildCount() - 1);
        i++;
        list.add(i);
        setRoomNo(list.size(),1);
    }

    private void setRoomNo(int list , int status) {
        Log.e("this", "Room Size "+list);
        textView = (TextView) rowView.findViewById(R.id.room_number);
        int a = list+1;
        textView.setText("Room " + a);
    }

    public void onDelete(View v) {

        Log.e("this","postion ::"+v.getParent());
        parentLinearLayout.removeView((View) v.getParent());
        i--;
        list.remove(list.size()-1);
        setRoomNo(list.size(),2);
    }


}
